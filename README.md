# Sphinx Builder

Container for build HTML and PDF documentation with sphinx.


- Sphinx
- Plantuml
- Latex (+RU lang)
- Sphinx RTD Theme
- Plugins: sphinx-copybutton, sphinxcontrib-plantuml, sphinxcontrib.video 
- make, curl, nexus3-cli