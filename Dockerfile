FROM ubuntu:20.04

ARG TZ=UTC

RUN set -ex \
	&& ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    # Update packages
    && apt update -yqq \
    # && apt upgrade -yqq \
    # Install system deps
    && apt install -yqq --no-install-recommends \
        # Sphinx
        python3-pip make graphviz imagemagick \
        # Plantuml
        plantuml \
        # PDF & Latex
        latexmk lmodern \
        texlive-latex-recommended texlive-latex-extra \
        texlive-fonts-recommended texlive-fonts-extra \
        texlive-lang-cyrillic \
        texlive-luatex texlive-xetex \
        # Upload&Utils
        curl git \
    # Clean apt
    && rm -rf \
      /var/lib/apt/lists/* \
      /tmp/* \
      /var/tmp/* \
      /usr/share/man \
      /usr/share/doc \
      /usr/share/doc-base \
    # Add telegram notifier
    && curl --output /usr/bin/telegram_notifier https://gitlab.com/smartgeosystem/utilities/telegram_notifier/-/package_files/4784406/download \
    && chmod +x /usr/bin/telegram_notifier

# Install python deps
COPY requirements.txt /opt/requirements.txt
RUN set -ex \
    && pip3 install --no-cache-dir -r /opt/requirements.txt
